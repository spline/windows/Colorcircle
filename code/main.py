#!/usr/bin/env python
import sys, socket
import tkinter as tk
from PIL import Image
screen = tk.Tk()
datastr = ""
host = "nom.spline.de"
port = 8443
im = Image.open("../images/colorcircle.png")
rgbIm = im.convert("RGB")
cc = tk.PhotoImage(file="../images/colorcircle.png")
screen.title("Colorcircle")
img = tk.Image("photo", file="../images/icon.png")
screen.call('wm','iconphoto',screen._w,img) 

def poweron():
    send("255 0 0\n")
    
def poweroff():
    send("0 0 0\n")
    
def rb():
    send("rb")
    
def rbc():
    send("rbc")
    
def cwr():
    send("cwr")
    
def cwg():
    send("cwg")
    
def cwb():
    send("cwb")
    
def trc():
    send("trc")
    
def tcr():
    send("tcr")

def tcg():
    send("tcg")

def tcb():
    send("tcb")
    
def entrydata():
    data = "0 0 0\n"
    datastr=var.get()
    st=datastr.split(" ")
    try:
        r = int(st[0])
        g = int(st[1])
        b = int(st[2])
        data = str(r) + " " + str(g) + " " + str(b) + "\n"
    except Exception:
        data = "255 0 0\n"
    send(data)

def leftclick(event):
    x = event.x
    y = event.y
    r,g,b = rgbIm.getpixel((x, y))
    data = str(r) + " " + str(g) + " " + str(b) + "\n"
    send(data)
    
def send(data):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((host, port))
    s.send(data.encode())
    s.close()

colorcircle = tk.Label(screen, image=cc, width=600, height=600)
colorcircle.bind("<Button-1>", leftclick)
colorcircle.pack(side="left")
bpoweron = tk.Button(screen, text="Power on", width=15, height=2, command = poweron).pack()
bpoweroff = tk.Button(screen, text="Power off", width=15, height=2, command = poweroff).pack()
border = tk.Label(screen, text="").pack()
label = tk.Label(screen, text="RGB:").pack()
var = tk.StringVar()
entry = tk.Entry(screen, textvariable=var, width=17).pack()
bsend = tk.Button(screen, text="Send", width=10, height=1, command = entrydata).pack()
btcb = tk.Button(screen, text="Chase blue", width=15, height=2, command = tcb).pack(side="bottom")
btcg = tk.Button(screen, text="Chase green", width=15, height=2, command = tcg).pack(side="bottom")
btcr = tk.Button(screen, text="Chase red", width=15, height=2, command = tcr).pack(side="bottom")
btrc = tk.Button(screen, text="TRB cycle", width=15, height=2, command = trc).pack(side="bottom")
bcwb = tk.Button(screen, text="Wipe blue", width=15, height=2, command = cwb).pack(side="bottom")
bcwg = tk.Button(screen, text="Wipe green", width=15, height=2, command = cwg).pack(side="bottom")
bcwr = tk.Button(screen, text="Wipe red", width=15, height=2, command = cwr).pack(side="bottom")
brbc = tk.Button(screen, text="Rainbow cycle", width=15, height=2, command = rbc).pack(side="bottom")
brb = tk.Button(screen, text="Rainbow", width=15, height=2, command = rb).pack(side="bottom")

screen.mainloop()
