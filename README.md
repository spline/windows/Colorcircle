Der Android Client (Java) ist hier zu finden: https://gitlab.spline.de/jrt/ColorCircleAndroid

Colorcircle sends RGB-Colors to manage the Spline-Logo 

The compiled binary is in release/bin

*cd release*

*cd bin*

*./colorcircle* 

If you want to run python file instead, you should install python3 and modules: sys, pillow, socket and tkinter (for example via pip)

*cd Colorcircle*

*cd code*

*./main.py*
